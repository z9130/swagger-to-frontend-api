use salvo::{
    oapi::{endpoint, extract::PathParam},
    Depot, Writer,
};
use rsns_common::{
    context::Context,
    error::{AppError, Error},
    request::JsonBody,
    res::{PageData, PageParams},
    response::AppResponse,
    transform,
};
use crate::model::user::*;
use crate::service::user_service;


/// 获取所有用户信息
#[endpoint(tags("系统-用户"), parameters(SysUserListInp))]
pub async fn get_page(
    depot: &mut Depot
) -> Result<AppResponse<CursorPageData<SysUserListModel>>, AppError>{
    let ctx = depot.obtain_mut::<Context>().unwrap();

    let result = user_service::page(ctx, page, params.to_owned()).await;
    Ok(AppResponse(result))
}

/// 获取单条用户信息
#[endpoint(tags("系统-用户"), parameters(SysUserViewInp))]
pub async fn add(
    depot: &mut Depot
) -> Result<AppResponse<()>, AppError>{
    let ctx = depot.obtain_mut::<Context>().unwrap();

    let result = user_service::find_by_id(ctx, page, params.to_owned()).await;
    Ok(AppResponse(result))
}

/// 新增用户信息
#[endpoint(tags("系统-用户"))]
pub async fn add(
    depot: &mut Depot
) -> Result<AppResponse<()>, AppError>{
    let ctx = depot.obtain_mut::<Context>().unwrap();

    let result = user_service::create(ctx, page, params.to_owned()).await;
    Ok(AppResponse(result))
}

/// 更新用户信息
#[endpoint(tags("系统-用户"))]
pub async fn update(
    depot: &mut Depot
) -> Result<AppResponse<()>, AppError>{
    let ctx = depot.obtain_mut::<Context>().unwrap();

    let result = user_service::update(ctx, page, params.to_owned()).await;
    Ok(AppResponse(result))
}

/// 删除用户信息
#[endpoint(tags("系统-用户"))]
pub async fn delete(
    depot: &mut Depot
) -> Result<AppResponse<()>, AppError>{
    let ctx = depot.obtain_mut::<Context>().unwrap();

    let result = user_service::delete(ctx, page, params.to_owned()).await;
    Ok(AppResponse(result))
}
