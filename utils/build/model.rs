

use salvo::oapi::{ToParameters, ToSchema};
use serde::{Deserialize, Serialize};
use validator::Validate;

#[derive(Serialize, Deserialize, Clone, Debug, Default, ToSchema)]
#[serde(rename_all = "camelCase")]
#[serde(default)]
pub struct SysUserListModel {
    pub id: String, 
    pub nickname: String, 
    pub username: String, 
    pub mobile: String, 
}

#[derive(Deserialize, Clone, Debug, Default, ToParameters, Validate)]
#[salvo(parameters(default_parameter_in = Query))]
pub struct SysUserListInp {
    pub id: Option<String>, 
    pub name: Option<String>, 
}


#[derive(Deserialize, Clone, Debug, Default, ToParameters, Validate)]
#[salvo(parameters(default_parameter_in = Query))]
pub struct SysUserViewInp {
    pub id: Option<String>, 
    pub name: Option<String>, 
}



#[derive(Serialize, Deserialize, Clone, Debug, Default, ToSchema, Validate)]
#[serde(rename_all = "camelCase")]
#[serde(default)]
pub struct SysUserAddInp {
    #[validate(length(min = 1, message = "user_id不得为空"))]
    pub user_id: int8, 
}


#[derive(Serialize, Deserialize, Clone, Debug, Default, ToSchema, Validate)]
#[serde(rename_all = "camelCase")]
#[serde(default)]
pub struct SysUserUpdateInp {
    pub user_id: Option<int8>, 
}


