const doT = require('dot');
const fs = require('fs');

// 读取模板文件
const template = fs.readFileSync('apiTemplate.dot', 'utf8');

// 编译模板
const tempFn = doT.template(template);

// 定义数据
const data = {
    apis: [
        { name: 'getUser', url: '/api/user' },
        { name: 'getPosts', url: '/api/posts' }
    ]
};

// 生成代码
const resultCode = tempFn(data);

// 输出或保存生成的代码
console.log(resultCode);
