#!/usr/bin/env node

import minimist from 'minimist';
import axios from 'axios';
import ejs from 'ejs';
import fs from 'fs';
import path from 'path';
import fetch from 'node-fetch';

import { generateTomlAPIFiles } from '../utils/tomlApi.js';
import { generateAPIFile } from '../gen-api/index.js';
// 解析命令行参数
const argv = process.argv.slice(2);
const argv_minimist = minimist(argv);


// http://127.0.0.1:3000/api-doc/openapi.json

// 打印解析结果
console.log("解析命令行参数:", argv, argv_minimist);

/**
 * @swagger_url http://127.0.0.1:3000/api-doc/openapi.json
 * @template ../templates/api.ejs
 * @output_dir ../src/api
 * @prefix_to_filter '/service/admin'
 */


// 必要参数检查
// if (!argv_minimist.swagger_url || !argv_minimist.template || !argv_minimist.output_dir) {
//     console.error('缺少必要的命令行参数，需要指定--swagger_url, --template, 与 --output_dir。');
//     process.exit(1);
// }



// const prefixToFilter = '/service/admin';
const prefix_to_filter = '/';

// contractReferType/{id}/list 输出 ["contractReferType,"list"]输入
function convertPathToArray(path) {
    return path.split('/').filter(segment => !segment.includes('{'));
}

function createNestedObject(data) {
    const result = {};

    Object.entries(data).forEach(([key, value]) => {
        const parts = key.split('.');
        let current = result;

        parts.forEach((part, index) => {
            if (!current[part]) {
                current[part] = (index === parts.length - 1) ? value : {};
            }
            current = current[part];
        });
    });

    return result;
}

// 生成API文件
async function generateAPIFiles(swaggerData, templatePath, outputDir) {

    const template = fs.readFileSync(templatePath, 'utf8');

    // 创建一个对象来按文件组织方法
    const files = {};

    // 通过路径方式生成
    if (argv_minimist.type == 'path') {
        let info = {}

        Object.entries(swaggerData.paths).forEach(([pathKey, methods]) => {
            // 检查路径是否包含指定的前缀
            if (pathKey.startsWith(argv_minimist.prefix_to_filter)) {

                // 移除前缀
                const newPathKey = pathKey.substring(argv_minimist.prefix_to_filter.length);

                // 获取路径部分，忽略最后的动作部分
                const pathParts = convertPathToArray(newPathKey);


                const action = pathParts.pop(); // 删除并获取最后一个元素作为动作
                const dirPath = pathParts.join('/'); // 重新组合剩余部分作为目录路径

                // 初始化路径对象
                if (!files[dirPath]) {
                    files[dirPath] = {};
                }
                console.log("pathParts:",newPathKey)
                console.log("pathParts:",pathParts)


                // 将方法添加到对应的路径对象
                // files[dirPath][action] = { path: newPathKey, methods };


                // console.log("actions:",files[dirPath][action])
    
                // console.log("actions:",files[dirPath][action])
                // console.log("actions:",newPathKey)

                // console.log("methods", newPathKey, JSON.stringify(methods))
                Object.entries(methods).forEach(([m, method]) => {
                    console.log("m",m)

                    method.path = '/app/'+newPathKey;
                    method.method = m;
                    method.tags = (method.tags||[])[0];
                    if(action){
                        info[`${pathParts.join('.')}.${m}_${action}`] = method
                    }

                    // console.log("method",method)

                    // files[dirPath][action].push({
                    //     path: newPathKey,
                    //     method: m,
                    //     showPath: `${m}_${action}`,
                    //     summary: methods[m].summary,
                    //     description: methods[m].description,
                    //     tags: methods[m].tags
                    // })
                })

            }
        });
    
        const nestedData = createNestedObject(info);
        console.log(JSON.stringify(nestedData))

        // console.error("==");

        // console.error(JSON.stringify(files));
        // console.error("=============");

        let filePath = path.join(outputDir, argv_minimist.file_name || `apiDefinitions.ts`);

        // console.error(JSON.stringify(files.upload));
        const apiContent = ejs.render(template, {
            functions: nestedData,
            // 首字母大写
            capitalizeFirstLetter: capitalizeFirstLetter
        });
        fs.writeFileSync(filePath, apiContent);


        return
        // 遍历files对象，为每个路径和方法集渲染ejs并输出文件
        Object.entries(files).forEach(([dirPath, actions]) => {
            const filePath = path.join(outputDir, `${dirPath}.ts`);

            // console.log(dirPath);
            // 确保目录存在
            // fs.mkdirSync(path.dirname(filePath), { recursive: true });

            // 使用ejs渲染模板


            // 写入渲染后的内容到对应文件
            // fs.writeFileSync(filePath, apiContent);
            // console.log(`Generated API for: ${dirPath}`);
        });
        return
    }

    // 通过 operationId 方式生成到一个文件中

    if (true) {
        console.log("通过 operationId 方式生成到一个文件中")
        let data = {}
        Object.entries(swaggerData.paths).forEach(([pathKey, methods]) => {
            console.log("pathKey:", pathKey)
            console.log("methods:", methods)

            // 检查路径是否包含指定的前缀
            if (pathKey.startsWith(argv_minimist.prefix_to_filter)) {
                // 移除前缀
                const newPath = pathKey.substring(argv_minimist.prefix_to_filter.length);

                Object.entries(methods).forEach(([methodKey, method]) => {
                    if (method.operationId) {
                        // 获取操作ID
                        let operationId = method.operationId;

                        if (operationId.indexOf("controller") == -1) {
                            console.log("operationId:", operationId)
                            if (method.tags && method.tags[0]) {
                                method.tags = method.tags[0].replace(/->/g, "-");
                            }
                            operationId = operationId.replace(/_/g, ".");
                            data[operationId] = {
                                method: methodKey.toUpperCase(),
                                path: newPath,
                                description: method.summary || method.description || '',
                                tags: method.tags,
                            }
                        }

                        // 创建路径对象
                    }
                })
            }


        })
        console.log(JSON.stringify(data))
        return
        // apiDefinitions


        const nestedData = createNestedObject(data);
        console.log(JSON.stringify(nestedData));

        // let filePath = path.join(outputDir, `apiDefinitions.ts`);
        let filePath = path.join(outputDir, argv_minimist.file_name || `apiDefinitions.ts`);

        // === 使用ejs渲染模板
        // export default {
        //     <% functions.forEach(function(fn) { %>
        //         /** <%= fn.tags %> <%= fn.description %> */
        //         "<%= fn.actionName %>": ["<%= fn.method %>","<%= fn.path %>"],
        //     <% }); %>
        //     }
        // const apiContent = ejs.render(fs.readFileSync('./template/apiDefinitions.ejs', 'utf8'), {
        //     functions: Object.entries(data).map(([operationId, methods]) => {
        //         return { actionName: operationId, ...methods };
        //     }),
        //     // 首字母大写
        //     capitalizeFirstLetter: capitalizeFirstLetter
        // });

        // const apiContent = ejs.render(fs.readFileSync('./template/apiDefinitions.ejs', 'utf8'), {

        const apiContent = ejs.render(fs.readFileSync(templatePath, 'utf8'), {
            functions: nestedData,
            // 首字母大写
            capitalizeFirstLetter: capitalizeFirstLetter
        });

        fs.writeFileSync(filePath, apiContent);
    }




}

async function readSwaggerFile(swaggerUrl) {
    if (swaggerUrl.startsWith('http')) {
        const response = await fetch(swaggerUrl);
        return await response.json();
    } else {
        const fileContent = fs.readFileSync(swaggerUrl, 'utf8');
        if (swaggerUrl.endsWith('.json')) {
            return JSON.parse(fileContent);
        }
        //   else if (swaggerUrl.endsWith('.yaml') || swaggerUrl.endsWith('.yml')) {
        //     return yaml.load(fileContent);
        //   }
    }
    throw new Error('Unsupported Swagger file format');
}

// 主逻辑
async function main() {
    if (argv_minimist.package == 'toml') {
        return generateTomlAPIFiles(argv_minimist)
    }

    if (argv_minimist.package == 'gen-api') {
        return generateAPIFile(argv_minimist)
    }

    const swaggerData = await readSwaggerFile(argv_minimist.swagger_url);
    console.log(swaggerData)
    await generateAPIFiles(swaggerData, argv_minimist.template, argv_minimist.output_dir);
}

main().catch(console.error);


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}